FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install apt-utils locales && \
    rm -rf /var/lib/apt/lists/*

RUN locale-gen en_US en_US.UTF-8
RUN update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
ENV LANG=en_US.UTF-8

RUN apt-get update && \
    apt-get -y install sudo vim nano net-tools curl gdb gdbserver gnupg2 \
    lsb-release bash-completion git cron && \
    rm -rf /var/lib/apt/lists/*

ENV ROS_DISTRO=dashing

RUN curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add -
RUN sh -c 'echo "deb [arch=amd64,arm64] http://packages.ros.org/ros2/ubuntu `lsb_release -cs` main" > /etc/apt/sources.list.d/ros2-latest.list'
RUN apt-get update && \
    apt-get -y install python3-colcon-common-extensions python3-argcomplete \
        ros-$ROS_DISTRO-ros-base ros-$ROS_DISTRO-rcl-interfaces \
        ros-$ROS_DISTRO-rclcpp-components ros-$ROS_DISTRO-ros2component \
        ros-$ROS_DISTRO-rosbag2 ros-$ROS_DISTRO-ros2bag \
        ros-$ROS_DISTRO-rosbag2-storage-default-plugins && \
    rm -rf /var/lib/apt/lists/*

ARG WDIR=/module
ARG UNAME=test
ARG HOST_UID=1000
ARG HOST_GID=1000

RUN groupadd -g $HOST_GID -o $UNAME
RUN useradd -m -u $HOST_UID -g $HOST_GID -o -s /bin/bash $UNAME
USER $UNAME

RUN echo "source /opt/ros/$ROS_DISTRO/setup.bash" >> ~/.bashrc
RUN echo "source $WDIR/install/setup.bash" >> ~/.bashrc

EXPOSE 2000
WORKDIR $WDIR

CMD ["/bin/bash"]
