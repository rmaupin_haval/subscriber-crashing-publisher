"""Launch in a component container."""

import launch
import launch_ros.actions
from launch_ros import get_default_launch_description
from launch_ros.actions import ComposableNodeContainer
from launch_ros.descriptions import ComposableNode
import datetime
import string
import os

def generate_launch_description():
    """Load Configuration File"""
    node_remaps=[]

    """Generate launch description with multiple components."""
    launch_description = get_default_launch_description()
    launch_description.add_action(
        ComposableNodeContainer(
            node_name='example',
            node_namespace=[],
            package='rclcpp_components',
            node_executable='component_container',
            composable_node_descriptions=[
                ComposableNode(
                    package='example',
                    node_plugin='ExamplePub',
                    node_name='pub',
                    remappings=node_remaps)
            ],
            output='screen',
        )
    )
    return launch_description
