#!/usr/bin/env bash

function print_usage() {
  RED='\033[0;31m'
  GREEN='\033[0;32m'
  BROWN='\033[0;33m'
  BLUE='\033[0;34m'
  BOLD='\033[1m'
  NONE='\033[0m'

  echo -e "\n${RED}Usage${NONE}: ./${BOLD}node.sh${NONE} <command>"
  echo -e "\n${RED}Commands${NONE}:
  ${BLUE}build${NONE}     : build
  ${BLUE}rebuild${NONE}   : clean and build
  ${BLUE}clean${NONE}     : removes all generated files
  ${BLUE}start${NONE}     : start module"
}

function start() {
  FILE=install/setup.bash
  if [ -f "$FILE" ]; then
    source $FILE
  else
    echo -e "\nBuild not found. Building source.\n"
    build
    source $FILE
  fi
  ros2 launch launch/launch.py
}

function build() {
  echo -e "\nBuilding.\n---------------------"
  source /opt/ros/$ROS_DISTRO/setup.bash
  colcon build
}

function clean() {
  echo -e "\nRemoving all generated files."
  rm -rf build/ log/ install/ launch/__pycache__/
}

case $1 in
build)
  build
  ;;
clean)
  clean
  ;;
rebuild)
  clean
  build
  ;;
start)
  start $2
  ;;
*)
  print_usage
  ;;
esac
