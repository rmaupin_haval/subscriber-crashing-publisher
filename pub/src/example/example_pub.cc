// Copyright 2019 American Haval Motor Technology.
// All Rights Reserved.
// CONFIDENTIAL - NOT FOR RELEASE
#include <chrono>
#include <cstdint>
#include <iostream>
#include <string>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/u_int32.hpp"

using std::chrono::milliseconds;

#define EX_MILLI 0
#define EX_PRINT 1000

class ExamplePub : public rclcpp::Node {
 public:
  explicit ExamplePub(const rclcpp::NodeOptions &options) : Node("pub", options) {
    publisher_ = this->create_publisher<std_msgs::msg::UInt32>("test", 10);
    auto callback = std::bind(&ExamplePub::Timer, this);
    timer_ = this->create_wall_timer(milliseconds(EX_MILLI), callback);
    counter_ = 0;
  }

 private:
  void Timer() {
    auto msg = std_msgs::msg::UInt32();
    msg.data = ++counter_;
    publisher_->publish(msg);
    if (counter_ % EX_PRINT == 0) {
      RCLCPP_INFO(this->get_logger(), "Sent %d", msg.data);
      std::flush(std::cout);
    }
  }

  uint32_t counter_;
  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::Publisher<std_msgs::msg::UInt32>::SharedPtr publisher_;

  RCLCPP_DISABLE_COPY(ExamplePub)
};

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(ExamplePub)
