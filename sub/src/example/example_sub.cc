// Copyright 2019 American Haval Motor Technology.
// All Rights Reserved.
// CONFIDENTIAL - NOT FOR RELEASE
#include <chrono>
#include <cstdint>
#include <iostream>
#include <string>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/u_int32.hpp"

using std::placeholders::_1;

#define EX_PRINT 100
#define EX_FAULT 1000

// rclcpp (ROS Client Library for C++)
class ExampleSub : public rclcpp::Node {
 public:
  explicit ExampleSub(const rclcpp::NodeOptions &options) : Node("sub", options) {
    auto callback = std::bind(&ExampleSub::ExampleCallback, this, _1);
    subscriber_ = this->create_subscription<std_msgs::msg::UInt32>("test", 10, callback);
  }
 private:
  void ExampleCallback(const std_msgs::msg::UInt32::SharedPtr msg) {
    //segfault immediately
    std_msgs::msg::UInt32 *segf;
    RCLCPP_INFO(this->get_logger(), "%d", segf->data);
  }
  rclcpp::Subscription<std_msgs::msg::UInt32>::SharedPtr subscriber_;
  RCLCPP_DISABLE_COPY(ExampleSub)
};

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(ExampleSub)
