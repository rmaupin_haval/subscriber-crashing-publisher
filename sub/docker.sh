#!/usr/bin/env bash

# Set Docker Image Tag
if [ -z $2 ]; then
    dockerTag=$(basename $PWD)
else
    dockerTag="$2"
fi

UNAME=test
SRC=$(readlink -f .)
DST=/module
CONTAINERS=

function print_usage() {
  RED='\033[0;31m'
  BLUE='\033[0;34m'
  BOLD='\033[1m'
  NONE='\033[0m'

  echo -e "\n${RED}Usage${NONE}: ./${BOLD}docker.sh${NONE} <command> [tag]"
  echo -e "Uses TAG '$dockerTag' by default."
  echo -e "\n${RED}Commands${NONE}:
  ${BLUE}build${NONE}: build docker
  ${BLUE}start${NONE}: enter docker
  ${BLUE}stop${NONE}: stop docker"
}

function build() {
  echo -e "Building docker..."
  docker build . -t $dockerTag \
    --build-arg HOST_UID=$(id -u) \
    --build-arg HOST_GID=$(id -g) \
    --build-arg UNAME=$UNAME \
    --build-arg WDIR=$DST
}

function load_container_ids() {
  CONTAINERS=$(docker ps -f name="$dockerTag" -q)
}

function start() {
  # Start docker if it doesn't exist
  load_container_ids

  if [ -z "$CONTAINERS" ]; then
    echo "Starting docker"
    docker run --net=host -it -d \
      $DOMAIN \
      -u $UNAME:$UNAME \
      --mount "type=bind,src=${SRC},dst=${DST}" \
      -w $DST --name $dockerTag \
      $dockerTag /bin/bash
    
    load_container_ids
  fi

  # Open existing docker
  docker exec -it ${CONTAINERS[0]} /bin/bash
}

function stop() {
  for id in $(docker ps -f ancestor="$dockerTag" -q); do
    echo "Stopping ${id}."
    docker kill $id > /dev/null
  done
}

# Run the selected command
case $1 in
  build)
    build
    ;;

  start)
    start
    ;;

  stop)
    stop
    ;;

  *)
    print_usage
    ;;
esac
