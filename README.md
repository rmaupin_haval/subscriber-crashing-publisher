# Subscriber crashing Publisher

This is an example project that reproduces the issue where the subscriber crashes the publisher.


# How to use.

1. Run `/docker.sh build` in each subfolder.
2. Run `/docker.sh start` in each subfolder, this will enter you into the docker, two console windows will be needed.
3. Run `/node.sh start` in `sub` docker.
4. Run `/node.sh start` in `pub` docker.
5. Wait for the error.
